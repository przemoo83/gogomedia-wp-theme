<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gogomedia
 */

get_header();
?>
    <header class="header">
        <section class="header-section">
            <div class="container">
                <div class="header__intro">
                    <h1>Heading</h1>
                    <p class="header__intro__paragraph">Lorem ipsum dolor sit amet, consect etur adipiscing elit. </p>
                </div>
                <div class="slider glide">
                    <div class="glide__track" data-glide-el="track">
                        <div class="glide__slides">
						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'slide' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>
                        </div>
                    </div>
                    <div data-glide-el="controls" class="slider-controls">
                        <span data-glide-dir="<" class="arrow left"></span>
                        <span data-glide-dir=">" class="arrow right"></span>
                    </div>
                </div>
            </div>
        </section>
    </header>
	<main>
        <section class="section">
            <div class="container">
                <h2>Heading</h2>
                <p>Lorem ipsum dolor sit amet, consect etur adipiscing elit.</p>
                <h3>Heading</h3>
                <div class="flex slider">
                    <div class="flex-block slider__block">

                        <h4>Heading</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                        <div class="slider__block__icon">
                            <img src=<?php echo get_template_directory_uri().'/assets/wp-logo.png' ?> alt="pic" />
                        </div>

                    </div>
                    <div class="flex-block slider__block">
                        <h4>Heading</h4>
                        <p>Lorem ipsum dolor sit amet, consect
                            etur adipiscing elit. Aenea uismod bibendum laoreet. Lorem ipsum dolor sit amet</p>
                    </div>
                    <div class="flex-block slider__block">
                        <h4>Heading</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php
get_sidebar();
get_footer();
